<?php


namespace App\Traits;


trait Calculate
{
    public function getTakeProfit(float $entryPrice, float $atr){
        return $entryPrice + $atr;
    }

    public function getStopLoss(float $entryPrice, float $atr){
        return $entryPrice - (1.5 * $atr);
    }

    public function getTrailingStopLoss(){

    }
}
