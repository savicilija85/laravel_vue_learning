create database lightbulb;
psql lightbulb -c 'create extension hstore;'
create user lightbulb with encrypted password 'dev';
grant all privileges on database lightbulb to lightbulb;
ALTER USER lightbulb WITH SUPERUSER;
CREATE EXTENSION hstore;