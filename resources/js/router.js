import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

import FirstPage from './components/pages/myFirstVuePage';
import NewRoutePage from './components/pages/newRoutePage';
import Hooks from './components/pages/basic/hooks';
import Methods from './components/pages/basic/methods';

//Project pages
import Home from './components/pages/home';
import Tags from './components/pages/tags';

const routes = [

    //Project routes
    {
        path: '/',
        component: Home
    },
    {
        path: '/tags',
        component: Tags
    },






    //Basic tutorial routes
    {
        path: '/my-new-vue-route',
        component: FirstPage
    },
    {
        path: '/new-route',
        component: NewRoutePage
    },
    //Vue hooks
    {
        path: '/hooks',
        component: Hooks
    },
    //More basics
    {
        path: '/methods',
        component: Methods
    },
];

export default new Router({
    mode: 'history',
    routes
});